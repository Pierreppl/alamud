from .effect import Effect2, Effect3
from mud.events import PayWithEvent

class PayWithEffect(Effect3):
    EVENT = PayWithEvent
