from .event import Event2
from .event import Event3
   
class PayWithEvent(Event3):
	NAME = "pay-with"
	
	def perform(self):
		if not self.object.has_prop("payable") or not self.object2.has_prop("money"):
			self.fail()
			return self.inform("pay-with.failed")
		self.inform("pay-with")
